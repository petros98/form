﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wallet.Classes;


namespace Wallet
{
    public partial class MainWindow : Window
    {

        User user = new User("user1", "Օգտատեր 1", 100000);
        
        public decimal temp=0;
        string tempName;
        Utilit u_name;
        public MainWindow()
        {             
            InitializeComponent();
            Display display = new Display();
            uName.Content = user.GetName();
            BtPay.Content = Strings.vcharel;
            userinfo.Text = Display.Disp( user.GetBalance());
        }

        #region click_Functions
        private void internet_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content =  Strings.partq + user.debts[Utilit.internet];
            utility_name.Content =tempName= Strings.internet;
            temp= user.debts[Utilit.internet];
            u_name = Utilit.internet;
        }

        private void electricty_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content = Strings.partq + user.debts[Utilit.electricity];
            temp = user.debts[Utilit.electricity];
            utility_name.Content =tempName = Strings.luys;
            u_name = Utilit.electricity;
        }

        private void fixTel_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content = Strings.partq + user.debts[Utilit.fixtel];
            utility_name.Content = tempName = Strings.fixtel;
            temp = user.debts[Utilit.fixtel];
            u_name = Utilit.fixtel;
        }

        private void gas_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content = Strings.partq + user.debts[Utilit.gas];
            utility_name.Content = tempName = Strings.gas;
            temp = user.debts[Utilit.gas];
            u_name = Utilit.gas;
        }

        private void mobile_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content = Strings.partq + user.debts[Utilit.mobile];
            utility_name.Content = tempName = Strings.mobile;
            temp = user.debts[Utilit.mobile];
            u_name = Utilit.mobile;
        }

        private void toBankAccount_Click(object sender, RoutedEventArgs e)
        {

        }

        private void water_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content = Strings.partq + user.debts[Utilit.water];
            utility_name.Content = tempName = Strings.jur;
            temp = user.debts[Utilit.water];
            u_name = Utilit.water;
        }

        private void damafon_Click(object sender, RoutedEventArgs e)
        {
            debt_info.Content = Strings.partq + user.debts[Utilit.domofon];
            utility_name.Content = tempName = Strings.domofon;
            temp = user.debts[Utilit.domofon];
            u_name = Utilit.domofon;
        }

        private void Pay_Click(object sender, RoutedEventArgs e)
        {
            if (temp!=0)
            {                       
                disp_History.Items.Add(tempName + "  "+ temp+ "  "+DateTime.Now);
                user.SetBalance(temp);
                user.debts[u_name] = 0;
                temp = 0;
               userinfo.Text= Display.Disp(user.GetBalance());
                debt_info.Content = Strings.partq + user.debts[u_name];
            }
        }
        #endregion
    }
}
