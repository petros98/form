﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallet.Interfaces
{
    interface IUser
    {    
      
        string GetName();
        decimal GetBalance();
        decimal GetCardBalance();
        void SetBalance(decimal d);
        void SetCardBalance(decimal d);
        
    }
}
