﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Interfaces;

namespace Wallet.Classes
{
    public class User : IUser
    {
        public User(string  user_name, string name, decimal bal)
        {
            User_Name = user_name;
            Random random = new Random();
            Name = name;
            Balance=CardBalance= bal;
            Utilit i;
            for ( i = Utilit.water; i <= Utilit.domofon; i++)
            {
                int r = random.Next(1000, 1500);

                Utility u = new Utility(i, r);
                
                debts.Add(i, u.GetDebt());
            }
           
        }

        public  User t;
        
        public Dictionary<Enum, decimal> debts = new Dictionary<Enum, decimal>();

        decimal CardBalance;
        decimal Balance;

        string User_Name;
        string Name;
        public void SetBalance(decimal bal)
        {
            Balance -= bal;
        }
        public void SetCardBalance(decimal bal)
        {
            CardBalance -= bal;
        }

        public decimal GetBalance()
        {
            return Balance;
        }

        public string GetName()
        {
            return Name;
        }

        public decimal GetCardBalance()
        {
            return CardBalance;
        }
    }
}
